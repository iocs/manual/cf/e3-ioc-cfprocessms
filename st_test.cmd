#require essioc
require opcua
require iocshutils

# Set EPICS environment variables

## Edit these as required for specific IOC requirements
## General IOC environment variables
epicsEnvSet(TOP, "$(E3_CMD_TOP)")
epicsEnvSet("PREFIX", "IOC_CFPROCESSMS")
epicsEnvSet("DEVICE", "Siemens DesigoCC")
epicsEnvSet("LOCATION", "cf-opc-02")
epicsEnvSet("ENGINEER", "Karl Vestin <karl.vestin@esss.se>"
epicsEnvSet("IOC_NAME", "$(PREFIX)")


# OPC environment variables
epicsEnvSet("SESSION",   "OPC1")
epicsEnvSet("SUBSCRIPT", "SUB1")

## OPC local test
epicsEnvSet("OPCSERVER", "localhost.localdomain")
epicsEnvSet("OPCPORT", "4840")

## Add one hour scan 
addScan 3600

## Load standard database
#iocshLoad $(essioc_DIR)/common_config.iocsh

# Load OPCUA module startup script
iocshLoad("$(opcua_DIR)/opcua.iocsh", "P=$(IOCNAME):OPC,SESS=$(SESSION),SUBS=$(SUBSCRIPT),INET=$(OPCSERVER),PORT=$(OPCPORT)")

## Load custom database
dbLoadTemplate("process.substitutions")
dbLoadTemplate("process_level_alarms.substitutions")

iocInit

