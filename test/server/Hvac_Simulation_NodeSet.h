/* WARNING: This is a generated file.
 * Any manual changes will be overwritten. */

#ifndef HVAC_SIMULATION_NODESET_H_
#define HVAC_SIMULATION_NODESET_H_


#ifdef UA_ENABLE_AMALGAMATION
# include "open62541.h"
#else
# include <open62541/server.h>
#endif



_UA_BEGIN_DECLS

extern UA_StatusCode Hvac_Simulation_NodeSet(UA_Server *server);

_UA_END_DECLS

#endif /* HVAC_SIMULATION_NODESET_H_ */
